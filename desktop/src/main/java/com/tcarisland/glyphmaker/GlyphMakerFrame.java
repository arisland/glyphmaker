package com.tcarisland.glyphmaker;

import java.awt.Component;
import java.awt.GraphicsConfiguration;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

public class GlyphMakerFrame extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5257208987455727043L;

	public GlyphMakerFrame() throws HeadlessException {
		super();
		initFrame();
	}

	public void initFrame() {
    	JMenuBar topMenu = new JMenuBar();
    	JMenu fileMenu = new JMenu("File");
    	JMenuItem openFileOption = new JMenuItem("Open File");
    	JFrame current = this;
    	openFileOption.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				JFileChooser chooser = new JFileChooser();
				int retval = chooser.showOpenDialog(current);
				if (retval == JFileChooser.APPROVE_OPTION) {
					
					JOptionPane.showMessageDialog(null, chooser.getSelectedFile().getAbsolutePath());
				}
			}
    		
    		
    	});
    	fileMenu.add(openFileOption);
    	topMenu.add(fileMenu);
    	this.setJMenuBar(topMenu);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(500, 500);
	}
	
	public GlyphMakerFrame(GraphicsConfiguration arg0) {
		super(arg0);
		initFrame();
	}

	public GlyphMakerFrame(String arg0, GraphicsConfiguration arg1) {
		super(arg0, arg1);
		initFrame();
	}

	public GlyphMakerFrame(String arg0) throws HeadlessException {
		super(arg0);
		initFrame();
	}

	
}
